<? include_once 'user.class.php';
  $books = $dbconn->allbooklist();
    if(isset($_POST['submit'])){
        $response = $dbconn->checkusername($_POST['username']);
        if(count($response)==0){
            $api_token= uniqid();
            if(isset($_POST['name'])){$name = $_POST['name'];}else{$name="";}
            if(isset($_POST['username'])){$username = $_POST['username'];}else{$username="";}
            if(isset($_POST['password'])){$password = $_POST['password'];}else{$password="";}

            if(isset($_POST['email'])){$email = $_POST['email'];}else{$email="";}
            if(isset($_FILES["image"]["name"])){$image = $_FILES["image"]["name"];$image_tempname = $_FILES["image"]["tmp_name"];}else{$image="";$image_tempname="";}

            $available = 1;
            $id =$dbconn->create($name,$username,$password,$email,$image,$image_tempname,$api_token,$available);
            if(isset($_POST['books'])){$books = $_POST['books'];
                for($i=0;$i<count($books);$i++){
                    $dbconn->addbook($books[$i],$id);
                }
            }
            if($id!="")
            {    
                header("Location: register.php?success");
            }
            else
            {
                header("Location: register.php?failure");
            }
        }else{
            header("Location: register.php?error");
        }
        
    }
?>
<!DOCTYPE html>
<html lang="en">
<? include_once 'css.php';?>
<body>
<? include_once 'header.php';?>

  <section id="hero" class="d-flex justify-cntent-center align-items-center">
    <div id="heroCarousel" class="container carousel carousel-fade" data-ride="carousel">

      <div class="carousel-item active">
        <div class="carousel-container">
          <h2 class="animate__animated animate__fadeInDown">Lorem Ipsum Dolor</h2>
          <p class="animate__animated animate__fadeInUp">Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus et tempore modi architecto.</p>
        </div>
      </div>
    </div>
  </section> 

  <main id="main">

    <!-- ======= Icon Boxes Section ======= -->
    <section id="icon-boxes" class="icon-boxes">
      
    </section>
    <section id="contact" class="contact">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Register</h2>
        </div>

        <div class="row mt-1 d-flex justify-content-center" data-aos="fade-center" data-aos-delay="100">

          

          <div class="col-lg-6 mt-5 mt-lg-0" data-aos="fade-center" data-aos-delay="100">
            <? if(isset($_GET['error'])){?>
                <div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    Username is already exists !
                </div>
            <? } else if(isset($_GET['success'])){?>
              <div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    Successfully registered !
                </div>
            <? } else if(isset($_GET['failure'])){?>
              <div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    Something went wrong! Try again.!
                </div>
            <? }?>
            <form action="register.php" method="post" role="form" enctype="multipart/form-data" id="register_form">
              <div class="form-row">
                <div class="col-md-12 form-group">
                  <input type="text" name="name" class="form-control required" id="name" placeholder="Your Name" minlength="2" />
                </div>
                <div class="col-md-12 form-group">
                  <input type="text" name="username" class="form-control required" id="username" placeholder="Your Username" minlength="2" />
                </div>
                <div class="col-md-12 form-group">
                  <input type="password" class="form-control required" name="password" id="password" placeholder="Your Password" minlength="2"/>
                </div>
                <div class="col-md-12 form-group">
                  <input type="text" minlength="2" class="form-control email required" name="email" id="email" placeholder="Your Email"/>
                </div>
                <div class="col-md-3 form-group">
                  <label><b> Profile Photo :</b></label>
                </div>
                <div class="col-md-9 form-group">
                    <input type="file" class="form-control required" id="image" name="image" accept=".png,.jpg,.jpeg">
                  <div class="invalid-feedback" id="imagemsg">Only .jpg,.jpeg,.png</div>
                </div>
                <div class="col-md-3 form-group">
                  <label><b> Select Books :</b></label>
                </div>
                <div class="col-md-9 form-group">
                  <select class="select2 form-select shadow-none mt-3 form-control required" name="books[]" multiple="multiple" style="height: 50px;width: 100%;">
                      <? foreach($books as $list){?>
                          <option value="<?echo $list['id'];?>"><?echo $list['title'];?></option>
                      <? }?>
                  </select>
                </div>
              </div>
              <div class="text-center"><button type="submit" class="btn btn-info" name="submit">Register</button></div>
              
            </form>
            <!-- <div class="text-left">
                <a class="btn btn-primary" href="register.php">Register Now!</a>
              </div> -->
          </div>

        </div>

      </div>
    </section>

  </main>
  <? include_once 'footer.php';?>
  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
  <div id="preloader"></div>
  <? include_once('js.php');?>

</body>

</html>
<script>
  $(document).ready(function()
  {
    $("#register_form").validate();
  });
</script>