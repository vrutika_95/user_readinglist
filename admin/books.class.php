<?php
require_once("database.php");

class Books extends database
{
    private $db;
    public function selectbook()
    {
        $query ='Select * from tbl_books order by id desc';
        $params = array();
        $result =$this->getall($query,$params);
        return $result;
    }
    public function create($title,$desc,$pages,$image,$image_tempname,$author,$publication,$available)
    {
        if($image!=""){
            $filename = $image;  $tempname  = $image_tempname;    
            $folder = "image/books/".$filename;
            move_uploaded_file($tempname, $folder);
        }
        $addUser = "INSERT INTO tbl_books(title,description,authorname,image,no_of_pages,publication,available) values(?,?,?,?,?,?,?)";
        $this->datamanupulation($addUser, [$title,$desc,$author,$image,$pages,$publication,$available]);
        return true;
  
    }
 
    public function getID($id)
    {
        $stmt = "SELECT * FROM tbl_books WHERE id=:id";
        $data =$this->get($stmt,[':id'=>$id]);
        return $data;
    }
 
    public function update($id,$title,$desc,$pages,$image,$image_tempname,$author,$publication,$available)
    {
        if($image!=""){
            $remvimg = $this->getID($id);
            unlink("image/books/".$remvimg['image']);
            
            $filename = $image;  $tempname  = $image_tempname;    
            $folder = "image/books/".$filename;
            move_uploaded_file($tempname, $folder);
            $query="UPDATE tbl_books SET title=:title,description=:description,authorname=:authorname,image=:image,no_of_pages=:no_of_pages,publication=:publication,available=:available WHERE id=:id ";
            $this->datamanupulation($query,[':id'=>$id,':title'=>$title,':description'=>$desc,':authorname'=>$author,':image'=>$image,':no_of_pages'=>$pages,':publication'=>$publication,':available'=>$available]);
        }else{
            $query="UPDATE tbl_books SET title=:title,description=:description,authorname=:authorname,no_of_pages=:no_of_pages,publication=:publication,available=:available WHERE id=:id ";
            $this->datamanupulation($query,[':id'=>$id,':title'=>$title,':description'=>$desc,':authorname'=>$author,':no_of_pages'=>$pages,':publication'=>$publication,':available'=>$available]);
        }
        
        
        return true; 
    }
 
    public function delete($id)
    {
        $remvimg = $this->getID($id);
        unlink("image/books/".$remvimg['image']);
        $query="DELETE FROM tbl_books WHERE id=:id";
        $this->datamanupulation($query,[':id'=>$id]);
        return true;
    }
 
 
    
   
}

$dbcon = new Books();

?>