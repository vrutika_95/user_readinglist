<?php 

    include_once 'books.class.php';
    include_once 'css.php';
   if(isset($_GET['detail_id'])){$details = $dbcon->getID($_GET['detail_id']);} 
?>
<div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"
    data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
    <? include_once 'header.php';?>
    <? include_once 'sidemenu.php';?>
    <div class="page-wrapper">
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-12 d-flex no-block align-items-center">
                    <h4 class="page-title">Book Details</h4>
                    <div class="ms-auto text-end">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item"><a href="books.php">Books</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Book Details</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
           
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title mb-0">Book Details</h5>
                        </div>
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td><b>Title</b> :</td>
                                    <td><? if($details['title']!=""){echo $details['title'];}else{echo '--';}?></td>
                                </tr>
                                <tr>
                                    <td><b>Descripion</b> :</td>
                                    <td><? if($details['description']!=""){echo $details['description'];}else{echo '--';}?></td>
                                </tr>
                                <tr>
                                    <td><b>Author</b> :</td>
                                    <td><? if($details['authorname']!=""){echo $details['authorname'];}else{echo '--';}?></td>
                                </tr>
                                <tr>
                                    <td><b>Image</b> :</td>
                                    <td><? if($details['image']!=""){?><img height="80px" width="80px" src='image/books/<? echo $details['image']?>'><?}else{echo '--';}?></td>
                                </tr>
                                <tr>
                                    <td><b>Pages</b> :</td>
                                    <td><? if($details['no_of_pages']!=""){echo $details['no_of_pages'];}else{echo '--';}?></td>
                                </tr>
                                <tr>
                                    <td><b>Publication</b> :</td>
                                    <td><? if($details['publication']!=""){echo $details['publication'];}else{echo '--';}?></td>
                                </tr>
                                <tr>
                                    <td><b>Likes</b> :</td>
                                    <td><? if($details['likes']!=""){echo $details['likes'];}else{echo '--';}?></td>
                                </tr>
                                <tr>
                                    <td><b>Is Available?</b> :</td>
                                    <td><? if($details['available']==1){echo 'Yes';}else{echo 'No';}?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <? //include_once 'footer.php';?>
    </div>
</div>
<? include_once 'js.php';?>   
<script>
     $().ready(function()
    {
        $("#book_form").valid();
    });
     /*var form = $("#book_form");
        form.valid();*/
</script>