<?php 

    include_once 'books.class.php';
    include_once 'css.php';
    $result =$dbcon->selectbook();
    if (isset($_GET['delete_id'])) {
        if($dbcon->delete($_GET['delete_id']))
        {    
            header("Location: books.php?deleted");
        }
        else
        {
            header("Location: books.php?error");
        }
    }
?>
<div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"
    data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
    <? include_once 'header.php';?>
    <? include_once 'sidemenu.php';?>
    <div class="page-wrapper">
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-12 d-flex no-block align-items-center">
                    <h4 class="page-title">Books List</h4>
                    <div class="ms-auto text-end">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Books</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
             <div class="row">
                <div class="col-12" align="right">
                    <a class="btn btn-info" href="add_book.php"><i class="fa fa-plus"></i> Add Book</a>
                </div>
            </div>
        </div>
        <div class="container-fluid">
           
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">

                            <? if(isset($_GET['inserted'])){?>
                                <div class="alert alert-success" role="alert">
                                    Record inserted successfully!
                                </div>
                            <? }else if(isset($_GET['updated'])){?>
                                <div class="alert alert-success" role="alert">
                                    Record updated successfully!
                                </div>
                            <? }else if(isset($_GET['deleted'])){?>
                                <div class="alert alert-success" role="alert">
                                    Record Deleted successfully!
                                </div>
                            <? }else if(isset($_GET['error'])){?>
                                <div class="alert alert-warning" role="alert">
                                    Something went Wrong!Can not Delete Record!
                                </div>
                            <? }?>
                            <div class="table-responsive">
                                <table id="zero_config" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>
                                                <label class="customcheckbox mb-3">
                                                    <input type="checkbox" id="mainCheckbox" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </th>
                                            <th>Title</th>
                                            <th>Author</th>
                                            <th>Image</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <? foreach($result as $list){?>
                                        <tr>
                                            <th>
                                                <label class="customcheckbox">
                                                    <input type="checkbox" class="listCheckbox" <? if($list['available']==1){echo "checked=checked";}?> disabled/>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </th>
                                            <td><? if($list['title']!=""){echo $list['title'];}else{echo '--';}?></td>
                                            <td><? if($list['authorname']!=""){echo $list['authorname'];}else{echo '--';}?></td>
                                            <td><? if($list['image']!=""){?><img height="80px" width="80px" src='image/books/<? echo $list['image']?>'> <?}else{echo '--';}?></td>
                                            <td><a href="update_book.php?edit_id=<? echo $list['id']?>"><i class="mdi mdi-pencil"></i></a> | <a href="details_book.php?detail_id=<? echo $list['id']?>"><i class="mdi mdi-file"></i></a> | <a href="books.php?delete_id=<? echo $list['id']?>"><i class="mdi mdi-delete"></i></a></td>
                                        </tr>
                                        <? }?>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <? //include_once 'footer.php';?>
    </div>
</div>
<? include_once 'js.php';?>   