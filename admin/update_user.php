<?php 

    include_once 'user.class.php';
    include_once 'css.php';

    $books = $dbconn->booklist();
   if(isset($_GET['edit_id'])){$details = $dbconn->getID($_GET['edit_id']);} 

    $selected_books = $dbconn->getSelectedbook($_GET['edit_id']);
    foreach ($selected_books as $value) {
        $selbooks[] = $value['book_id'];
    }
    //print_r($selected_books);die;
    if(isset($_POST['id'])){
        $response = $dbconn->checkusernamebyis($_POST['username'],$_POST['id']);
        if(isset($_POST['id'])){$id = $_POST['id'];}else{$id="";}
        if(isset($_POST['name'])){$name = $_POST['name'];}else{$name="";}
        if(isset($_POST['username'])){$username = $_POST['username'];}else{$username="";}

        if(isset($_POST['email'])){$email = $_POST['email'];}else{$email="";}
        if(isset($_FILES["image"]["name"])){$image = $_FILES["image"]["name"];$image_tempname = $_FILES["image"]["tmp_name"];}else{$image="";$image_tempname="";}

        if(isset($_POST['available']) || $_POST['available']=='available'){$available = 1;}else{$available=0;}
        if(isset($_POST['books'])){$books = $_POST['books'];
            $dbconn->del_book($id);
            for($i=0;$i<count($books);$i++){
                $dbconn->addbook($books[$i],$id);
            }
        }
        
        if($dbconn->update($id,$name,$username,$email,$image,$image_tempname,$available))
        {    
            header("Location: users.php?updated");
        }
        else
        {
            header("Location: update_user.php?edit_id=".$_POST['id']."?failure");
        }
    }
?>
<div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"
    data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
    <? include_once 'header.php';?>
    <? include_once 'sidemenu.php';?>
    <div class="page-wrapper">
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-12 d-flex no-block align-items-center">
                    <h4 class="page-title">Update User</h4>
                    <div class="ms-auto text-end">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item"><a href="users.php">Users</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Update User</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
           
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">

                            <? if(isset($_GET['failure'])){?>
                                <div class="alert alert-warning" role="alert">
                                    Something went Wrong!Can not Update Record!
                                </div>
                            <? } else if(isset($_GET['exist'])){?>
                                <div class="alert alert-warning" role="alert">
                                    Username is already exists..!
                                </div>
                            <? }?>
                            <form class="form-horizontal" name="user_form" method="post" id="user_form" class="mt-5" enctype="multipart/form-data" action="update_user.php">
                                <div class="card-body">
                                     <div class="form-group row">
                                        <label for="name" class="col-sm-3 text-end control-label col-form-label">Name *</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" required="required" value="<? echo $details['name']?>" id="name" name="name" 
                                                placeholder="Name">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="username" class="col-sm-3 text-end control-label col-form-label">Username *
                                        </label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" value="<? echo $details['username']?>" required="required" id="username" name="username" placeholder="Name">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label for="email" class="col-sm-3 text-end control-label col-form-label">Email *</label>
                                        <div class="col-sm-9">
                                            <input type="email" class="form-control" id="email" value="<? echo $details['email']?>" required="required" name="email" placeholder="Email">
                                        </div>
                                    </div>

                                    
                                    <div class="form-group row">
                                        <label for="image" class="col-sm-3 text-end control-label col-form-label">Profile Photo </label>
                                        <div class="col-sm-9">
                                            <input type="file" class="custom-file-input" id="image" name="image" 
                                                     accept=".png,.jpg,.jpeg">
                                            <div class="invalid-feedback" id="imagemsg">Only .jpg,.jpeg,.png</div><br>
                                            <? if($details['image']!=""){?><img height="80px" width="80px" src='image/users/<? echo $details['image']?>'> <?}?>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="books" class="col-sm-3 text-end control-label col-form-label">Select Books * </label>
                                        <div class="col-sm-9">
                                            <select class="select2 form-select shadow-none mt-3" name="books[]" required="required" multiple="multiple"
                                                style="height: 36px;width: 100%;">
                                                <? foreach($books as $list){?>
                                                    <option value="<?echo $list['id'];?>" <? if(in_array($list['id'],$selbooks)){echo "selected";}?>><?echo $list['title'];?></option>
                                                <? }?>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label for="publication" class="col-sm-3 text-end control-label col-form-label"> </label>
                                        <div class="col-sm-9">
                                            
                                            <input id="available" name="available" type="checkbox" value="available" <? if($details['available']==1){echo "checked='checked'";}?>>
                                            <label for="available">Is Available?</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="border-top">
                                    <div class="card-body">
                                        <input type="hidden" name="id" value="<? echo $details['id'];?>">
                                        <button type="submit" class="btn btn-primary" name="submit">Submit</button>
                                        <a href="users.php" class="btn btn-primary" name="cancel">Cancel</a>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <? //include_once 'footer.php';?>
    </div>
</div>
<? include_once 'js.php';?>   
<script>
     $().ready(function()
    {
        $("#user_form").validate();
    });
    $("#image").change(function() 
    {
        var val = $(this).val();
        switch(val.substring(val.lastIndexOf('.') + 1).toLowerCase())
        {
            case 'png': case 'jpg':case 'jpeg':
                $("#imagemsg").hide();
                break;
            default:
                $(this).val('');
                $("#imagemsg").show();
                document.getElementById("imagemsg").style.color = "red";
                $("#imagemsg").html('.png,.jpeg,.jpg Only');
                break;
        }
    });
</script>