<?php 

    include_once 'books.class.php';
    include_once 'css.php';
    if(isset($_POST['submit'])){
        if(isset($_POST['title'])){$title = $_POST['title'];}else{$title="";}
        if(isset($_POST['desc'])){$desc = $_POST['desc'];}else{$desc="";}
        if(isset($_POST['pages'])){$pages = $_POST['pages'];}else{$pages="";}
        if(isset($_FILES["image"]["name"])){$image = $_FILES["image"]["name"];$image_tempname = $_FILES["image"]["tmp_name"];}else{$image="";$image_tempname="";}
        if(isset($_POST['author'])){$author = $_POST['author'];}else{$author="";}
        if(isset($_POST['publication'])){$publication = $_POST['publication'];}else{$publication="";}

        if(isset($_POST['available']) || $_POST['available']=='available'){$available = 1;}else{$available=0;}
        
        if($dbcon->create($title,$desc,$pages,$image,$image_tempname,$author,$publication,$available))
        {    
            header("Location: books.php?inserted");
        }
        else
        {
            header("Location: add_book.php?failure");
        }
    }
?>
<div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"
    data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
    <? include_once 'header.php';?>
    <? include_once 'sidemenu.php';?>
    <div class="page-wrapper">
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-12 d-flex no-block align-items-center">
                    <h4 class="page-title">Add Books</h4>
                    <div class="ms-auto text-end">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item"><a href="books.php">Books</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Add Books</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
           
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">

                            <? if(isset($_GET['failure'])){?>
                                <div class="alert alert-warning" role="alert">
                                    Something went Wrong!Can not add Record!
                                </div>
                            <? } ?>
                            <form class="form-horizontal" name="book_form" method="post" id="book_form" class="mt-5" enctype="multipart/form-data" action="add_book.php">
                                <div class="card-body">
                                    <h4 class="card-title">Personal Info</h4>
                                    <div class="form-group row">
                                        <label for="title" class="col-sm-3 text-end control-label col-form-label">Title *</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" minlength="2" required="required" id="title" name="title" 
                                                placeholder="Title Here">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="desc" class="col-sm-3 text-end control-label col-form-label">Description *
                                        </label>
                                        <div class="col-sm-9">
                                            <textarea class="form-control" id="desc" minlength="2" name="desc" required="required" placeholder="Description"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="author" minlength="2" class="col-sm-3 text-end control-label col-form-label">Author *</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="author" required="required" name="author" 
                                                placeholder="Author">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="image" class="col-sm-3 text-end control-label col-form-label">Image *</label>
                                        <div class="col-sm-9">
                                            <input type="file" class="custom-file-input" id="image" name="image" 
                                                    required accept=".png,.jpg,.jpeg">
                                            <div class="invalid-feedback" id="imagemsg">Only .jpg,.jpeg,.png</div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="pages" class="col-sm-3 text-end control-label col-form-label">Pages </label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" minlength="2" id="pages" name="pages" 
                                                placeholder="Pages">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="publication" class="col-sm-3 text-end control-label col-form-label">Publication </label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" minlength="2" id="publication" name="publication" 
                                                placeholder="Publication">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="publication" class="col-sm-3 text-end control-label col-form-label"> </label>
                                        <div class="col-sm-9">
                                            
                                            <input id="available" name="available" type="checkbox" value="available">
                                            <label for="available">Is Available?</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="border-top">
                                    <div class="card-body">
                                        <button type="submit" class="btn btn-primary" name="submit">Submit</button>
                                        <a href="books.php" class="btn btn-primary" name="cancel">Cancel</a>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <? //include_once 'footer.php';?>
    </div>
</div>
<? include_once 'js.php';?>   
<script>
     $().ready(function()
    {
        $("#book_form").validate();
    });
    $("#image").change(function() 
    {
        var val = $(this).val();
        switch(val.substring(val.lastIndexOf('.') + 1).toLowerCase())
        {
            case 'png': case 'jpg':case 'jpeg':
                $("#imagemsg").hide();
                break;
            default:
                $(this).val('');
                $("#imagemsg").show();
                document.getElementById("imagemsg").style.color = "red";
                $("#imagemsg").html('.png,.jpeg,.jpg Only');
                break;
        }
    });
</script>