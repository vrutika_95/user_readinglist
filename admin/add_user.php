    <script type="text/javascript" src="http://ajax.microsoft.com/ajax/jquery.validate/1.7/jquery.validate.js"></script>
<?php 

    include_once 'user.class.php';
    include_once 'css.php';
    $books = $dbconn->booklist();
    if(isset($_POST['submit'])){
        $response = $dbconn->checkusername($_POST['username']);
        if(count($response)==0){
            $api_token= uniqid();
            if(isset($_POST['name'])){$name = $_POST['name'];}else{$name="";}
            if(isset($_POST['username'])){$username = $_POST['username'];}else{$username="";}
            if(isset($_POST['password'])){$password = $_POST['password'];}else{$password="";}

            if(isset($_POST['email'])){$email = $_POST['email'];}else{$email="";}
            if(isset($_FILES["image"]["name"])){$image = $_FILES["image"]["name"];$image_tempname = $_FILES["image"]["tmp_name"];}else{$image="";$image_tempname="";}

            if(isset($_POST['available']) || $_POST['available']=='available'){$available = 1;}else{$available=0;}
            $id =$dbconn->create($name,$username,$password,$email,$image,$image_tempname,$api_token,$available);
            if(isset($_POST['books'])){$books = $_POST['books'];
                for($i=0;$i<count($books);$i++){
                    $dbconn->addbook($books[$i],$id);
                }
            }
            if($id!="")
            {    
                header("Location: users.php?inserted");
            }
            else
            {
                header("Location: add_user.php?failure");
            }
        }else{
            header("Location: add_user.php?exists");
        }
        
    }

?>
<div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"
    data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
    <? include_once 'header.php';?>
    <? include_once 'sidemenu.php';?>
    <div class="page-wrapper">
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-12 d-flex no-block align-items-center">
                    <h4 class="page-title">Add User</h4>
                    <div class="ms-auto text-end">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item"><a href="users.php">Books</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Add Books</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
           
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">

                            <? if(isset($_GET['failure'])){?>
                                <div class="alert alert-warning" role="alert">
                                    Something went Wrong!Can not add Record!
                                </div>
                            <? } else if(isset($_GET['exist'])){?>
                                <div class="alert alert-warning" role="alert">
                                    Username is already exists..!
                                </div>
                            <? }?>
                            <form class="form-horizontal" name="user_form" method="post" id="user_form" class="mt-5" enctype="multipart/form-data" action="add_user.php">
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label for="name" class="col-sm-3 text-end control-label col-form-label">Name *</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" required="required" id="name" name="name" 
                                                placeholder="Name">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="username" class="col-sm-3 text-end control-label col-form-label">Username *
                                        </label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" required="required" id="username" name="username" placeholder="User Name">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="password" class="col-sm-3 text-end control-label col-form-label">Password * </label>
                                        <div class="col-sm-9">
                                            <input type="password" class="form-control" required="required" id="password" name="password" placeholder="Password">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="email" class="col-sm-3 text-end control-label col-form-label">Email *</label>
                                        <div class="col-sm-9">
                                            <input type="email" class="form-control" id="email" required="required" name="email" placeholder="Email">
                                        </div>
                                    </div>

                                    
                                    <div class="form-group row">
                                        <label for="image" class="col-sm-3 text-end control-label col-form-label">Profile Photo *</label>
                                        <div class="col-sm-9">
                                            <input type="file" class="custom-file-input" id="image" name="image" 
                                                    required="required" accept=".png,.jpg,.jpeg">
                                            <div class="invalid-feedback" id="imagemsg">Only .jpg,.jpeg,.png</div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="books" class="col-sm-3 text-end control-label col-form-label">Select Books * </label>
                                        <div class="col-sm-9">
                                            <select class="select2 form-select shadow-none mt-3" name="books[]" required="required" multiple="multiple"
                                                style="height: 36px;width: 100%;">
                                                <? foreach($books as $list){?>
                                                    <option value="<?echo $list['id'];?>"><?echo $list['title'];?></option>
                                                <? }?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="publication" class="col-sm-3 text-end control-label col-form-label"> </label>
                                        <div class="col-sm-9">
                                            
                                            <input id="available" name="available" type="checkbox" value="available">
                                            <label for="available">Is Available?</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="border-top">
                                    <div class="card-body">
                                        <button type="submit" class="btn btn-primary" name="submit">Submit</button>
                                        <a href="users.php" class="btn btn-primary" name="cancel">Cancel</a>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <? //include_once 'footer.php';?>
    </div>
</div>
<? include_once 'js.php';?>   
<script>
     $().ready(function()
    {
        $("#user_form").validate();
    });

    
    $("#image").change(function() 
    {
        var val = $(this).val();
        switch(val.substring(val.lastIndexOf('.') + 1).toLowerCase())
        {
            case 'png': case 'jpg':case 'jpeg':
                $("#imagemsg").hide();
                break;
            default:
                $(this).val('');
                $("#imagemsg").show();
                document.getElementById("imagemsg").style.color = "red";
                $("#imagemsg").html('.png,.jpeg,.jpg Only');
                break;
        }
    });
    
</script>