<?php 

    include_once 'user.class.php';
    include_once 'css.php';
   if(isset($_GET['detail_id'])){$details = $dbconn->getID($_GET['detail_id']);} 
   $selected_books = $dbconn->getSelectedbook($_GET['detail_id']);
   foreach ($selected_books as $value) {
       $sel[] =$value['name'];
       $selectedbook = implode(' , ',$sel) ;
   }
?>
<div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"
    data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
    <? include_once 'header.php';?>
    <? include_once 'sidemenu.php';?>
    <div class="page-wrapper">
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-12 d-flex no-block align-items-center">
                    <h4 class="page-title">User Details</h4>
                    <div class="ms-auto text-end">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item"><a href="users.php">Users</a></li>
                                <li class="breadcrumb-item active" aria-current="page">User Details</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
           
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title mb-0">User Details</h5>
                        </div>
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td><b>Name</b> :</td>
                                    <td><? if($details['name']!=""){echo $details['name'];}else{echo '--';}?></td>
                                </tr>
                                <tr>
                                    <td><b>Username</b> :</td>
                                    <td><? if($details['username']!=""){echo $details['username'];}else{echo '--';}?></td>
                                </tr>
                                <tr>
                                    <td><b>Email</b> :</td>
                                    <td><? if($details['email']!=""){echo $details['email'];}else{echo '--';}?></td>
                                </tr>
                                <tr>
                                    <td><b>Image</b> :</td>
                                    <td><? if($details['image']!=""){?><img height="80px" width="80px" src='image/users/<? echo $details['image']?>'><?}else{echo '--';}?></td>
                                </tr>
                                <tr>
                                    <td><b>Books</b> :</td>
                                    <td><? if(!empty($selected_books)){echo $selectedbook;}else{echo '--';}?></td>
                                </tr>
                                <tr>
                                    <td><b>Is Available?</b> :</td>
                                    <td><? if($details['available']==1){echo 'Yes';}else{echo 'No';}?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <? //include_once 'footer.php';?>
    </div>
</div>
<? include_once 'js.php';?>   
<script>
     $().ready(function()
    {
        $("#book_form").valid();
    });
     /*var form = $("#book_form");
        form.valid();*/
</script>