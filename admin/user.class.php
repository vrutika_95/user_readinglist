<?php
require_once("database.php");

class User extends database
{
    private $db;
    
    public function selectuser()
    {
        $query ='Select * from tbl_user';
        $params = array();
        $result =$this->getall($query,$params);
        return $result;
    }

    public function booklist(){
        $query= "SELECT * FROM tbl_books WHERE available=:available";
        //$params = array(['available']=>1);
        $result =$this->getall($query,[':available'=>1]);
        return $result;
    }
    public function create($name,$username,$password,$email,$image,$image_tempname,$api_token,$available)
    {
        if($image!=""){
            $filename = $image;  $tempname  = $image_tempname;    
            $folder = "image/users/".$filename;
            move_uploaded_file($tempname, $folder);
        }
        $addUser = "INSERT INTO tbl_user(name,username,password,email,image,api_token,available) values(?,?,?,?,?,?,?)";
        $this->datamanupulation($addUser, [$name,$username,$password,$email,$image,$api_token,$available]);
        return $this->lastInsetedId();
  
    }
    public function checkusername($username){
        $stmt = "SELECT * FROM tbl_user where username=:username";
        $data =$this->getAll($stmt,[':username'=>$username]);
       return $data;
    }

    public function checkusernamebyid($username,$id){
        $stmt = "SELECT * FROM tbl_user where username=:username and id!=:id";
        $data =$this->getAll($stmt,[':username'=>$username,':id'=>$id]);
       return $data;
    }
    public function addbook($book_id,$user_id)
    {
        $addUser = "INSERT INTO tbl_user_books(book_id,user_id) values (?,?)";
        $this->datamanupulation($addUser, [$book_id,$user_id]);
        return true;
  
    }
 
    public function getID($id)
    {
        $stmt = "SELECT * FROM tbl_user WHERE id=:id";
        $data =$this->get($stmt,[':id'=>$id]);
        return $data;
    }
 
    public function update($id,$name,$username,$email,$image,$image_tempname,$available)
    {
        if($image!=""){
            $remvimg = $this->getID($id);
            unlink("image/users/".$remvimg['image']);
            
            $filename = $image;  $tempname  = $image_tempname;    
            $folder = "image/users/".$filename;
            move_uploaded_file($tempname, $folder);
            $query="UPDATE tbl_user SET name=:name,username=:username,email=:email,image=:image,available=:available WHERE id=:id ";
            $this->datamanupulation($query,[':id'=>$id,':name'=>$name,':username'=>$username,':email'=>$email,':image'=>$image,':available'=>$available]);
        }else{
             $query="UPDATE tbl_user SET name=:name,username=:username,email=:email,available=:available WHERE id=:id ";
            $this->datamanupulation($query,[':id'=>$id,':name'=>$name,':username'=>$username,':email'=>$email,':available'=>$available]);
        }
        return true; 
    }
 
    public function delete($id)
    {
        $remvimg = $this->getID($id);
        unlink("image/users/".$remvimg['image']);
        $query="DELETE FROM tbl_user WHERE id=:id";
        $this->datamanupulation($query,[':id'=>$id]);
        $query1="DELETE FROM tbl_user_books WHERE user_id=:id";
        $this->datamanupulation($query1,[':id'=>$id]);
        return true;
    }
 
    public function del_book($id)
    {
        $query="DELETE FROM tbl_user_books WHERE user_id=:id";
        $this->datamanupulation($query,[':id'=>$id]);
        return true;
    }
    
    public function getSelectedbook($id)
    {
        $stmt = "SELECT *,(select title from tbl_books where tbl_books.id=tbl_user_books.book_id) as name FROM tbl_user_books WHERE user_id=:id";
        $data =$this->getall($stmt,[':id'=>$id]);
        return $data;
    }
    
}

$dbconn = new User();

?>