<? include_once 'user.class.php';
  if(isset($_GET['user_id'])){$books_list = $dbconn->booklist($_GET['user_id']);}
  if (isset($_POST['liked'])) {
    $book_id = $_POST['book_id'];
    $user_id = $_POST['user_id'];
    $liked = $dbconn->getbook($book_id);
    $n = $liked['likes'];
    $insert_like = $dbconn->insert_like($user_id,$book_id);
    $update_likes = $dbconn->update_likes($n+1,$book_id);
    /*mysqli_query($con, "INSERT INTO likes (userid, postid) VALUES (1, $postid)");
    mysqli_query($con, "UPDATE posts SET likes=$n+1 WHERE id=$postid");*/

    echo $n+1;
    exit();
  }
  if (isset($_POST['unliked'])) {
    $book_id = $_POST['book_id'];
        $user_id = $_POST['user_id'];
    $liked = $dbconn->getbook($book_id);
    $n = $liked['likes'];

    $insert_like = $dbconn->remove_like($user_id,$book_id);
    $update_likes = $dbconn->update_likes($n-1,$book_id);
    echo $n-1;
    exit();
  }

  // Retrieve posts from the database
  $pos
?>
<!DOCTYPE html>
<html lang="en">
<? include_once 'css.php';?>
<body>
<? include_once 'header.php';?>

  <section id="hero" class="d-flex justify-cntent-center align-items-center">
    <div id="heroCarousel" class="container carousel carousel-fade" data-ride="carousel">

      <div class="carousel-item active">
        <div class="carousel-container">
          <h2 class="animate__animated animate__fadeInDown">Lorem Ipsum Dolor</h2>
          <p class="animate__animated animate__fadeInUp">Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus et tempore modi architecto.</p>
        </div>
      </div>
    </div>
  </section> 

  <main id="main">

    <!-- ======= Icon Boxes Section ======= -->
    <section id="icon-boxes" class="icon-boxes">
      
    </section><!-- End Icon Boxes Section -->

    <section id="team" class="team section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Books</h2>
        </div>

        <div class="row">
          <? if(count($books_list)>0){
            foreach($books_list as $list){
              $bookname = $dbconn->getbook($list['book_id']);
          ?>
            <div class="col-lg-6" data-aos="fade-up" data-aos-delay="100">
              <div class="member d-flex align-items-start">
                <div class="pic"><img src="admin/image/books/<? echo $bookname['image'];?>" class="img-fluid" alt=""></div>
                <div class="member-info">
                  <h4><? echo $bookname['title'];?></h4>
                  <span> <? echo $bookname['authorname'];?></span>
                  <p><? echo $bookname['description'];?></p>
                  <p><i class="ri-pages-fill"></i> Pages : <? echo $bookname['no_of_pages'];?></p>
                  <p><i class="ri-book-fill"></i> Publication : <? echo $bookname['publication'];?></p>
                  
                    <? $results = $dbconn->likes($list['user_id'],$list['book_id']);
                    if (count($results) == 1 ){ ?>
                      <!-- user already likes post -->
                      <i style="display: none;" class="ri-thumb-up-fill like" id="like" data-id="<?php echo $list['book_id']; ?>" data-user_id="<?php echo $list['user_id']; ?>"></i>
                        <i class="ri-thumb-down-fill unlike" data-id="<?php echo $list['book_id']; ?>" data-user_id="<?php echo $list['user_id']; ?>"></i> 
                    <?php }else{ ?>
                      <!-- user has not yet liked post -->
                      <i class="ri-thumb-up-fill like" data-id="<?php echo $list['book_id']; ?>" data-user_id="<?php echo $list['user_id']; ?>"></i> 
                      <? //if($bookname['likes']!= '0'){?>
                        <i class="ri-thumb-down-fill unlike" style="display: none;" id="unlike" data-id="<?php echo $list['book_id']; ?>" data-user_id="<?php echo $list['user_id']; ?>"></i> 
                      <? //}?>
                    <?php } ?>

                  <span class="likes_count"><?php echo $bookname['likes']; ?> likes</span>
                
                </div>
              </div>
            </div>
          <? }
          }?>
        </div>

      </div>
    </section>

  </main>
  <? include_once 'footer.php';?>
  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
  <div id="preloader"></div>
  <? include_once('js.php');?>

</body>

</html>
<script>
  $(document).ready(function(){
    // when the user clicks on like
    var sess = '<? echo $_SESSION['username']?>';
    if(sess!=''){
    $('.like').on('click', function(){
      var book_id = $(this).data('id');
      var user_id = $(this).data('user_id');
          $book = $(this);

      $.ajax({
        url: 'books_list.php',
        type: 'post',
        data: {
          'liked': 1,
          'book_id': book_id,
          'user_id': user_id,
        },
        success: function(response){
          $book.parent().find('span.likes_count').text(response + " likes");
          $book.addClass('hide');
          $book.siblings().removeClass('hide');
          $('#unlike').show();
          $('#like').hide();
        }
      });
    });

    // when the user clicks on unlike
    $('.unlike').on('click', function(){
      var book_id = $(this).data('id');
      var user_id = $(this).data('user_id');
        $book = $(this);

      $.ajax({
        url: 'books_list.php',
        type: 'post',
        data: {
          'unliked': 1,
          'book_id': book_id,
          'user_id': user_id,
        },
        success: function(response){
          $book.parent().find('span.likes_count').text(response + " likes");
          $book.addClass('hide');
          $('#unlike').hide();
          $('#like').show();
          $book.siblings().removeClass('hide');
        }
      });
    });
  }
  });
</script>