<?
  if (!isset($_SESSION)) {
        session_start();
   }
?>
<header id="header" class="fixed-top ">
  <div class="container d-flex align-items-center">

    <h1 class="logo mr-auto"><a href="#header" class="scrollto">User's Reading List</a></h1>
    
    <nav class="nav-menu d-none d-lg-block">
      <ul>
        
        
        <? if(isset($_SESSION['loggedin']) && $_SESSION['loggedin']=='yes'){?>
          <li class="active"><a href="#header">Home</a></li>
          <li><a href="logout.php">Log Out</a></li>
       <? }else{?>
          <li class="active"><a href="index.php">Home</a></li>  
          <li><a href="login.php">Log In</a></li>
          <li><a href="register.php">Register</a></li>
        <? }?>
        <li><a href="admin">Admin</a></li>
      </ul>
    </nav><!-- .nav-menu -->

  </div>
</header>