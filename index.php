<? include_once 'user.class.php';
  $users = $dbconn->userlist();
?>
<!DOCTYPE html>
<html lang="en">
<? include_once 'css.php';?>
<body>
<? include_once 'header.php';?>

  <section id="hero" class="d-flex justify-cntent-center align-items-center">
    <div id="heroCarousel" class="container carousel carousel-fade" data-ride="carousel">

      <div class="carousel-item active">
        <div class="carousel-container">
          <h2 class="animate__animated animate__fadeInDown">Lorem Ipsum Dolor</h2>
          <p class="animate__animated animate__fadeInUp">Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus et tempore modi architecto.</p>
        </div>
      </div>
    </div>
  </section><!-- End Hero -->

  <main id="main">

    <!-- ======= Icon Boxes Section ======= -->
    <section id="icon-boxes" class="icon-boxes">
      
    </section><!-- End Icon Boxes Section -->

    <section id="team" class="team section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Users</h2>
        </div>

        <div class="row">
          
          <? if(count($users)>0){
            foreach($users as $list){
          ?>
            <div class="col-lg-6 mt-4 mt-lg-0" data-aos="fade-up">
              <div class="member d-flex align-items-start">
                <div class="pic"><img src="admin/image/users/<? echo $list['image'];?>" class="img-fluid" alt=""></div>
                <div class="member-info">
                  <a href="books_list.php?user_id=<? echo $list['id'];?>"><h4><? echo $list['name'];?></h4></a>
                  <span> </span>
                  <p><i class="ri-mail-fill"></i> : <? echo $list['email'];?></p>
                </div>
              </div>
            </div>
          <? }
          }?>
        </div>

      </div>
    </section>

  </main>
  <? include_once 'footer.php';?>
  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
  <div id="preloader"></div>
  <? include_once('js.php');?>

</body>

</html>