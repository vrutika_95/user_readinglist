<?php
require_once("database.php");

class User extends database
{
    private $db;
    
    public function userlist(){
        $query= "SELECT * FROM tbl_user WHERE available=:available";
        //$params = array(['available']=>1);
        $result =$this->getall($query,[':available'=>1]);
        return $result;
    }
 
    public function booklist($id)
    {
        $stmt = "SELECT * FROM tbl_user_books WHERE user_id=:id";
        $data =$this->getall($stmt,[':id'=>$id]);
        return $data;
    }

    public function allbooklist(){
        $query= "SELECT * FROM tbl_books WHERE available=:available";
        //$params = array(['available']=>1);
        $result =$this->getall($query,[':available'=>1]);
        return $result;
    }
    public function create($name,$username,$password,$email,$image,$image_tempname,$api_token,$available)
    {
        if($image!=""){
            $filename = $image;  $tempname  = $image_tempname;    
            $folder = "image/users/".$filename;
            move_uploaded_file($tempname, $folder);
        }
        $addUser = "INSERT INTO tbl_user(name,username,password,email,image,api_token,available) values(?,?,?,?,?,?,?)";
        $this->datamanupulation($addUser, [$name,$username,$password,$email,$image,$api_token,$available]);
        return $this->lastInsetedId();
  
    }

    public function update_password($username,$password)
    {
        $addUser = "UPDATE tbl_user SET password=:password WHERE username=:username";
        $this->datamanupulation($addUser, [':username'=>$username,':password'=>$password]);
        return true;
  
    }
    public function checkusername($username){
        $stmt = "SELECT * FROM tbl_user where username=:username";
        $data =$this->getAll($stmt,[':username'=>$username]);
       return $data;
    }
    public function addbook($book_id,$user_id)
    {
        $addUser = "INSERT INTO tbl_user_books(book_id,user_id) values (?,?)";
        $this->datamanupulation($addUser, [$book_id,$user_id]);
        return true;
  
    }
    public function likes($user_id,$book_id)
    {
        $stmt = "SELECT * FROM tbl_likes WHERE user_id=:user_id and book_id=:book_id";
        $data =$this->getall($stmt,[':user_id'=>$user_id,':book_id'=>$book_id]);
        return $data;
    }
 
    public function getbook($id)
    {
        $stmt = "SELECT * FROM tbl_books WHERE id=:id";
        $data =$this->get($stmt,[':id'=>$id]);
        return $data;
    }
 
    public function login($username,$password)
    {
        $stmt = "SELECT * FROM tbl_user WHERE username=:username and password=:password and available=:available";
        $data =$this->get($stmt,[':username'=>$username,':password'=>$password,':available'=>1]);
        return $data;
    }
 
    public function del_book($id)
    {
        $query="DELETE FROM tbl_user_books WHERE user_id=:id";
        $this->datamanupulation($query,[':id'=>$id]);
        return true;
    }
    
    public function getSelectedbook($id)
    {
        $stmt = "SELECT *,(select title from tbl_books where tbl_books.id=tbl_user_books.book_id) as name FROM tbl_user_books WHERE user_id=:id";
        $data =$this->getall($stmt,[':id'=>$id]);
        return $data;
    }

    public function insert_like($user_id,$book_id){
        $query = "Insert into tbl_likes(user_id,book_id) values (?,?)";
        $this->datamanupulation($query,[$user_id,$book_id]);
        return true;
    }

    public function update_likes($likes,$book_id){
        $query = "Update tbl_books SET likes=:likes where id=:id";
        $this->datamanupulation($query,[':likes'=>$likes,':id'=>$book_id]);
        return true;
    }

    public function remove_like($user_id,$book_id){
        $query = "DELETE from tbl_likes where book_id=:book_id and user_id=:user_id";
        $this->datamanupulation($query,[':book_id'=>$book_id,':user_id'=>$user_id]);
        return true;
    }
    
}

$dbconn = new User();

?>