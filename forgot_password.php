<? include_once 'user.class.php';
  if (isset($_POST['submit'])) {
   $response = $dbconn->checkusername($_POST['username']);
      if(count($response)!=0){
        $update = $dbconn->update_password($_POST['username'],$_POST['password']);
        if ($update == true) {
          header('Location: forgot_password.php?success');
        }else{
          header('Location: forgot_password.php?failure');
        }
      }else{
        header('Location: forgot_password.php?error');
      }
  }
?>
<!DOCTYPE html>
<html lang="en">
<? include_once 'css.php';?>
<body>
<? include_once 'header.php';?>

  <section id="hero" class="d-flex justify-cntent-center align-items-center">
    <div id="heroCarousel" class="container carousel carousel-fade" data-ride="carousel">

      <div class="carousel-item active">
        <div class="carousel-container">
          <h2 class="animate__animated animate__fadeInDown">Lorem Ipsum Dolor</h2>
          <p class="animate__animated animate__fadeInUp">Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus et tempore modi architecto.</p>
        </div>
      </div>
    </div>
  </section> 

  <main id="main">

    <!-- ======= Icon Boxes Section ======= -->
    <section id="icon-boxes" class="icon-boxes">
      
    </section>
    <section id="contact" class="contact">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Forgot Password</h2>
        </div>

        <div class="row mt-1 d-flex justify-content-center" data-aos="fade-center" data-aos-delay="100">

          

          <div class="col-lg-6 mt-5 mt-lg-0" data-aos="fade-center" data-aos-delay="100">
            <? if(isset($_GET['failure'])){?>
                <div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    Something went wrong !
                </div>
            <? } else if(isset($_GET['error'])){?>
                <div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    Username is not available !
                </div>
            <? } else if(isset($_GET['success'])){?>
                <div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    Password updated successfully !
                </div>
            <? }?>
            <form action="forgot_password.php" method="post" role="form" id="forgotpassword_form" >
              <div class="form-row">
                <div class="col-md-12 form-group">
                  <input type="text" name="username" class="form-control required" id="username" placeholder="Your Username"/>
                  <div class="validate" style="color: #FF0000"></div>
                </div>
                <div class="col-md-12 form-group">
                  <input type="password" class="form-control required" name="password" id="password" placeholder="Your New Password">
                  <div class="validate" style="color: #FF0000"></div>
                </div>
              </div>
              <div class="text-center"><button type="submit" class="btn btn-info" name="submit">Submit</button></div>
              
            </form>
          </div>

        </div>

      </div>
    </section>

  </main>
  <? include_once 'footer.php';?>
  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
  <div id="preloader"></div>
  <? include_once('js.php');?>

</body>

</html>
<script>
  $(document).ready(function()
  {
    $("#forgotpassword_form").validate();
  });
</script>